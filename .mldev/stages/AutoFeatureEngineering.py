import pandas as pd
import numpy as np
import featuretools as ft
from sklearn.preprocessing import LabelEncoder

from mldev.experiment import experiment_tag


@experiment_tag()
class FeatureSynthesis(object):
    def __init__(self, dataframe: pd.DataFrame, target: pd.DataFrame, target_value: str, index_id: str):
        self.df = dataframe
        self.target = target
        self.target_value = target_value
        self.index_id = index_id
        self.numeric_features = []
        self.entity_set_name = "basic"
        self.entity_name = "basic_entity"
        self.agg_primitives = ["sum", "max", "min", "mean", "count"]
        self.feature_matrix = pd.array([])
        self.feature_names = []
        self.max_depth = 3
        self.synthesized = pd.array([])

    def encode(self):
        Encoder = LabelEncoder()
        if self.target.dtype != np.number or len(self.target.unique()) < 40:
            self.target = Encoder.fit_transform(self.target)
        for column in self.df.columns:
            if self.df[column].dtype == np.number:
                if len(self.df[column].unique()) > 20:
                    self.df[column] = self.df[column].replace(np.nan, np.mean(self.df[column]))
                    self.numeric_features.append(column)
                else:
                    self.df[column] = Encoder.fit_transform(self.df[column])
            elif self.df[column].dtype == "object":
                try:
                    self.df[column] = Encoder.fit_transform(self.df[column])
                except TypeError:
                    self.df = self.df.drop(columns=column)

    def synthesis(self):
        EntitySet = ft.EntitySet(id=self.entity_set_name)
        EntitySet = EntitySet.entity_from_dataframe(entity_id=self.entity_name, dataframe=self.df, index=self.index_id)
        for col in self.numeric_features:
            EntitySet = EntitySet.normalize_entity(base_entity_id=self.entity_name, new_entity_id=col, index=col)
        self.feature_matrix, self.feature_names = ft.dfs(entityset=EntitySet, target_entity=self.entity_name,
                                                         agg_primitives=self.agg_primitives,
                                                         max_depth=self.max_depth, features_only=False, verbose=False)
        corr = self.feature_matrix.corr().abs()
        upper = corr.where(np.triu(np.ones(corr.shape), k=1).astype(np.bool))
        threshold = 0.65
        collinear_features = [column for column in upper.columns if any(upper[column] > threshold)]
        self.feature_matrix = self.feature_matrix.drop(self.feature_matrix[collinear_features], axis=1)

    def synthesed_dataset(self):
        return self.feature_matrix, self.target

    def write_data(self, path):
        self.synthesized.to_csv("synthesed.csv")
