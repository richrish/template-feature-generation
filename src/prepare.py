import argparse
import pickle
from sklearn.model_selection import train_test_split
import pandas as pd
import sys
import os
from utils import make_dir, DATA_PATH
sys.path.append(os.getcwd() + '/.mldev/stages')
from AutoFeatureEngineering import *
from QuadProgFeatureSelection import *

def load_and_split():
    target_value = "Loan_Status"
    indexID = "Loan_ID"
    df = pd.read_csv("loan.csv", delimiter=';')
    feature_num = 13
    value_df = df[target_value]
    df = df.drop(columns=target_value)
    feat_process = FeatureSynthesis(df, value_df, target_value, indexID)
    feat_process.encode()
    feat_process.synthesis()
    feat_matrix, target_vec = feat_process.synthesed_dataset()
    print(len(feat_matrix.columns))
    id_df = df[indexID]
    selector = QPFS(feat_matrix, target_vec, target_value, id_df, indexID, feature_num)
    selector.solve_qp()
    new_df, Y = selector.selected_dataset()
    X_pretrain, X_test, y_pretrain, y_test = train_test_split(new_df, Y, test_size=0.2)
    X_train, X_dev, y_train, y_dev = train_test_split(X_pretrain, y_pretrain, test_size=0.2)
    return X_train, X_dev, X_test, y_train, y_dev, y_test


def save_data(X_train, X_dev, X_test, y_train, y_dev, y_test):
    def do_pickle(obj, path):
        with open(path, "wb") as fp:
            pickle.dump(obj, fp)

    do_pickle(X_train, f"{DATA_PATH}/X_train.pickle")
    do_pickle(X_dev, f"{DATA_PATH}/X_dev.pickle")
    do_pickle(X_test, f"{DATA_PATH}/X_test.pickle")
    do_pickle(y_train, f"{DATA_PATH}/y_train.pickle")
    do_pickle(y_dev, f"{DATA_PATH}/y_dev.pickle")
    do_pickle(y_test, f"{DATA_PATH}/y_test.pickle")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--run_name", default="")
    run_name = parser.parse_args().run_name

    global DATA_PATH
    DATA_PATH = f"./data/{run_name}"

    make_dir(f"{DATA_PATH}")
    X_train, X_dev, X_test, y_train, y_dev, y_test = load_and_split()
    save_data(X_train, X_dev, X_test, y_train, y_dev, y_test)


if __name__ == "__main__":
    main()
